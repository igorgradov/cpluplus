/*
 * Integer.h
 *
 *  Created on: 25 Jul 2014
 *      Author: Tel-Ran2
 */

#ifndef INTEGER_H_
#define INTEGER_H_

/*
 *
 */
class IntegerBit;
class Integer 
{
public:
	int m_a;
public:
	Integer(int a=0):m_a(a){};
	virtual ~Integer();
	operator int ()const{
		return m_a;
	}
	bool operator[](unsigned int index)const
	{
		unsigned int max_index=sizeof(int)*8-1;
		if(index>max_index) index=max_index;
		return (((unsigned int)m_a)>>index)&1;
	}
	operator[](unsigned int index)
};
class IntegerBit{
Integer* pInt;
unsigned int m_nBit;
public:
operator bool();
Integer& operator=(bool value);
IntegerBit(Integer* pInt, unsigned int nBit):m_pInt(pInt),m_nBit(nBit);

#endif /* INTEGER_H_ */
