

#include <iostream>
#include <cstring>

namespace my
{
	class string
	{
	private:
		char* m_cstr;
		size_t m_clenght;
		void _setText(const char* cstr, char* & dest);
	public:
		string();
		string(const char*);
		void setText(const char* cstr);
		const char* getText() const;
	};
}


